# piball

## Setup

### build and install the pybox2d submodule

```
# make sure we have all the prerequisites
sudo apt-get install build-essential python-dev swig python-pygame

# init the pybox2d submodule
git submodule update --init

# this will take a while and you can ignore the warnings
cd pybox2d
python3 setup.py build
sudo python3 setup.py install
```

### install dependencies

```
pip3 install -r requirements.txt
```

### connecting the sensor

![wiring](https://tutorials-raspberrypi.de/wp-content/uploads/GY-521_accelerometer_Gyroscope_Steckplatine-524x500.png)

Activate i2c on the pi

```
sudo raspi-config
```

then go to Interfacing Options and enable I2C
(under Advanced on older versions)

open the modules file

```
sudo nano /etc/modules
```

and add the following lines (if not already included)

```
i2c-bcm2708
i2c-dev
```

save it and reboot with

```
sudo reboot
```

test the sensor with

```
sudo i2cdetect -y 1
```

you should get

```
 0 1 2 3 4 5 6 7 8 9 a b c d e f
00: -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- 68 -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

So the 0x68 is the address for the sensor

make sure you have python-smbus installed

```
sudo apt install python3-smbus
```

## Run

```
./start.sh
```

## Tiled Editor

Download from [https://www.mapeditor.org/](https://www.mapeditor.org/)

### Generate object types file

```
python -m piball.editor.gentypes
```

View > Object Types Editor > File > Choose Object Types File...

Then select the objecttypes.xml file in piball/editor/

Now when you add a new entity, make sure you run the above command and Tiled will auto reload the types file.

### Installing the editor extension

```
cd piball/editor/piballeditor
npm i
npm run build
gulp installExtension
```

The extension looks for a layer group called "\_walls" then applies boolean operations to the "\_add" and "\_sub" layers within it. When you save the file, the extension processes the boolean layers into a "walls_output" layer that can be loaded by the engine.
If the extension finds invalid geometry, it will highlight it in red.
