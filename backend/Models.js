const mongoose = require('mongoose');
    
var UserModel = mongoose.model("user", {
    name: String,
    score: String
});

module.exports = UserModel