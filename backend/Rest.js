var UserModel = require("./Models");

function initAPIRoutes(app) {
	app.get("/", (req, res) => {
		console.log("hello");
	});

	app.get("/score", (req, res) => {
		response = {};

		UserModel.find({}, (err, docs) => {
			if (err) console.log(err);
			else {
				docs.forEach(obj => {
					response[obj.name] = obj.score;
				});

				res.json(response);
			}
		});
    });
    
    app.post("/score/new", (req,res) => {
        UserModel.updateOne({name:req.body.name} , req.body, {upsert:true}, (err)=>{
            if(err) console.log(err);

            else{
                res.status(201).send();
                
            }
        });

        
	});
	
	app.get("/*", (req,res)=>{
		res.redirect("/");
	})

}

module.exports = initAPIRoutes;
