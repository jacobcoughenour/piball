require('dotenv').config()
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const app = express();
const port = process.env.PORT || 8080;

const mongoose = require('mongoose');

mongoose.connect(String(process.env.MONGODB_URI), {useNewUrlParser:true, useUnifiedTopology: true });

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => {
	console.log("Connected to MongoDB!");
})

// full path to this file as an array
let fullpath = path.dirname(__filename).split(path.sep);

// go to the project root
fullpath = fullpath.slice(0, fullpath.length - 1);

// path to client build dir
const clientpath = path.join(fullpath.join(path.sep), "frontend", "build");

// Serve the static files from the React app
app.use(express.static(clientpath));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const server = app.listen(port, () => console.log("Listening on port", port));

var api = require('./Rest')

api(app);