import React from "react";
import "./App.css";
import {
	BrowserRouter,
	Switch,
	Route,
	NavLink,
	useLocation,
	Redirect
} from "react-router-dom";
import { AppBar, Tabs, Tab, Grid, Typography, Link } from "@material-ui/core";
import { useTheme, withStyles } from "@material-ui/styles";

import HomePage from "./pages/HomePage";
import HighScoresPage from "./pages/HighScoresPage";
import WeeklyReportsPage from "./pages/WeeklyReportsPage";
import FilesPage from "./pages/FilesPage";

const maxWidth = 1080;

const pages = {
	"/": { name: "Home", exact: true, component: <HomePage /> },
	"/scores": { name: "Scoreboard", component: <HighScoresPage /> },
	"/reports": { name: "Weekly Reports", component: <WeeklyReportsPage /> },
	"/files": { name: "Files", component: <FilesPage /> }
};

function App() {
	const theme = useTheme();

	return (
		<div
			className="App"
			style={{
				background: theme.palette.background.default,
				color: theme.palette.text.primary
			}}
		>
			{/* creates react-router context */}
			<BrowserRouter>
				{/* top navigation bar */}
				<NavBar className="navbar" />

				{/* page content container */}
				<div
					className="content"
					style={{
						maxWidth: maxWidth
					}}
				>
					<Switch>
						{Object.keys(pages).map((path, key) => (
							<Route key={key} path={path} exact={!!pages[path].exact}>
								{pages[path].component || <></>}
							</Route>
						))}

						{/* if url doesn't match any of the pages
							we redirect to the homepage  */}
						<Route path="/">
							<Redirect to="/"></Redirect>
						</Route>
					</Switch>
					<Switch>
						<Route path="/">
							<Footer></Footer>
						</Route>
					</Switch>
				</div>
			</BrowserRouter>
		</div>
	);
}

const NavBar = withStyles(theme => ({
	root: {
		background: "inherit",
		display: "flex",
		alignItems: "center",
		boxShadow: "0px 0px 8px 4px " + theme.palette.background.default
	},
	tabs: {
		marginRight: 24
	},
	tab: {
		minWidth: 64,
		marginRight: 16
	}
}))(props => {
	const { classes } = props;

	let location = useLocation();
	let currentPageIndex = 0;
	const tabs = Object.keys(pages).map((path, key) => {
		if (path === location.pathname) currentPageIndex = key;
		return (
			<Tab
				className={classes.tab}
				disableRipple
				key={key}
				component={NavLink}
				to={path}
				label={pages[path].name}
			></Tab>
		);
	});
	return (
		<AppBar className={classes.root} elevation={0} position="fixed">
			<div
				style={{
					display: "flex",
					padding: "8px 0",
					flex: 1,
					maxWidth: maxWidth,
					width: "100%"
				}}
			>
				<Logo />
				<div
					style={{
						flex: 1,
						display: "flex",
						justifyContent: "flex-end"
					}}
				>
					<CustomTabs className={classes.tabs} value={currentPageIndex}>
						{tabs}
					</CustomTabs>
				</div>
			</div>
		</AppBar>
	);
});

const Logo = () => {
	return (
		<NavLink
			to="/"
			style={{
				flex: 0,
				display: "inline-block",
				verticalAlign: "middle",
				textAlign: "center",
				height: 40,
				padding: "0 12px 0 28px",
				lineHeight: "48px",
				userSelect: "none",
				borderRadius: 4,
				color: "white",
				textDecoration: "none"
			}}
		>
			<div className="logo">piball</div>
		</NavLink>
	);
};

const CustomTabs = withStyles(theme => ({
	indicator: {
		display: "flex",
		justifyContent: "center",
		backgroundColor: "transparent",
		"& > div": {
			maxWidth: 40,
			width: "100%",
			backgroundColor: theme.palette.secondary.main
		}
	}
}))(props => <Tabs {...props} TabIndicatorProps={{ children: <div /> }} />);

const Footer = () => {
	return (
		<footer
			style={{
				padding: "28px 0 48px 0",
				overflow: "hidden",
				width: "100%"
			}}
		>
			<Grid container spacing={4} direction="row">
				<Grid item xs={3} style={{ opacity: 0.66 }}>
					<Logo></Logo>
				</Grid>
				<Grid
					item
					sm
					container
					style={{
						marginTop: 8
					}}
				>
					<Grid item xs={3}>
						<Typography>Pages</Typography>
						{Object.keys(pages).map((path, key) => {
							return (
								<NavLink
									key={key}
									to={path}
									style={{
										textDecoration: "none"
									}}
								>
									<Typography color="primary">{pages[path].name}</Typography>
								</NavLink>
							);
						})}
					</Grid>
					<Grid item>
						<Typography>More</Typography>
						<Typography>
							<Link
								target="_blank"
								href="https://gitlab.com/jacobcoughenour/piball"
							>
								GitLab Repo
							</Link>
						</Typography>
						<Typography>
							<Link
								target="_blank"
								href="https://gitlab.com/jacobcoughenour/piball/issues"
							>
								Issue Board
							</Link>
						</Typography>
					</Grid>
				</Grid>
			</Grid>
		</footer>
	);
};

export default App;
