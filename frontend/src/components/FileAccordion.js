import React from "react";
import { ExpansionPanel } from "@material-ui/core";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

export default class Accordion extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div>
				<ExpansionPanel>
					<ExpansionPanelSummary
						expandIcon={<ExpandMoreIcon />}
						aria-label="Expand"
						aria-controls="additional-actions1-content"
						id="additional-actions1-header"
					>
						<Typography
							style={{
								fontWeight: "bold",
								fontSize: "20px"
							}}
						>
							Week {this.props.week}:
						</Typography>
					</ExpansionPanelSummary>

					<ExpansionPanelDetails style={{ flexDirection: "column" }}>
						<Typography color="textSecondary">Accomplished Tasks</Typography>
						<div style={{ textIndent: "10px" }}> {this.props.tasks}</div>
						<Typography color="textSecondary">Unfinished Tasks</Typography>
						<div style={{ textIndent: "10px" }}>{this.props.unfinished}</div>
					</ExpansionPanelDetails>
				</ExpansionPanel>
			</div>
		);
	}
}
