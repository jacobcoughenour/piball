import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { ThemeProvider } from "@material-ui/core";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import * as serviceWorker from "./serviceWorker";
import { blueGrey, lightBlue } from "@material-ui/core/colors";

// create material-ui theme
const theme = createMuiTheme({
	palette: {
		type: "dark",
		primary: blueGrey,
		secondary: lightBlue,
		background: {
			default: "#263238",
			paper: "#37474F"
		}
	}
});

ReactDOM.render(
	<ThemeProvider theme={theme}>
		<App />
	</ThemeProvider>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
