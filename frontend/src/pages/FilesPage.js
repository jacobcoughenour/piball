import React from "react";
import {
	Paper,
	Typography,
	Link,
	List,
	ListItem,
	ListItemIcon,
	ListItemText
} from "@material-ui/core";
import { Code, InsertDriveFileOutlined } from "@material-ui/icons";

export default class FilesPage extends React.Component {
	render() {
		return (
			<>
				<div
					style={{
						textAlign: "center"
					}}
				>
					<Typography
						variant="h4"
						style={{
							paddingTop: 28
						}}
					>
						Project Files and Resources
					</Typography>
				</div>

				<div
					style={{
						marginTop: 48,
						display: "flex",
						alignItems: "center",
						flexDirection: "column"
					}}
				>
					<List>
						<ListItem
							button
							component="a"
							target="_blank"
							href="https://gitlab.com/jacobcoughenour/piball"
						>
							<ListItemIcon>
								<Code />
							</ListItemIcon>
							<ListItemText>Source Code on GitLab</ListItemText>
						</ListItem>

						<ListItem
							button
							component="a"
							target="_blank"
							href="https://frostburg-my.sharepoint.com/:w:/g/personal/jrcoughenour0_frostburg_edu/ER9-2T8YcahBo21O8uv1qUQBFT_eZ7MZfQO2Gh-9N2gOuw?e=r4FWjv"
						>
							<ListItemIcon>
								<InsertDriveFileOutlined />
							</ListItemIcon>
							<Typography>Research Paper</Typography>
						</ListItem>
						<ListItem
							button
							component="a"
							target="_blank"
							href="https://docs.google.com/document/d/12Z50s7ljffFpyYP84NAu6lOlJEjDrUGMsRoeO7kJSKw/edit?usp=sharing"
						>
							<ListItemIcon>
								<InsertDriveFileOutlined />
							</ListItemIcon>
							<Typography>Final Report Paper</Typography>
						</ListItem>
						<ListItem
							button
							component="a"
							target="_blank"
							href="https://frostburg-my.sharepoint.com/:p:/g/personal/jrcoughenour0_frostburg_edu/EcPsXGtigk9Kh7QGXy_hFWEBNSh2wpb4vNXnHd85YBRdqA?e=8yFPIl"
						>
							<ListItemIcon>
								<InsertDriveFileOutlined />
							</ListItemIcon>
							<Typography>Presentation Slides</Typography>
						</ListItem>
					</List>
				</div>
			</>
		);
	}
}
