import React from "react";
import "./HighScoresPage.css";
import { Paper, Typography, CircularProgress } from "@material-ui/core";

export default class HighScoresPage extends React.Component {
	constructor(props) {
		super();
		this.state = {
			loading: true,
			error: false,
			scores: []
		};
	}

	componentDidMount() {
		this.fetchScores();
	}

	render() {
		return (
			<>
				<div
					style={{
						textAlign: "center"
					}}
				>
					<Typography
						variant="h4"
						style={{
							paddingTop: 28
						}}
					>
						Player High Scores
					</Typography>
				</div>

				<Paper style={{ marginTop: 48 }}>{this.renderInside()}</Paper>
			</>
		);
	}

	renderInside() {
		if (this.state.loading) {
			return <CircularProgress style={{ marginTop: 25 }}></CircularProgress>;
		}

		if (this.state.error)
			return (
				<>
					<Typography align="center">error fetching scores</Typography>
					<Typography align="center">{this.state.error.message}</Typography>
				</>
			);

		return (
			<div className="score-list">
				{this.state.scores.map((e, key) => (
					<div className="score" key={key}>
						{`${e.name} : ${e.score}`}
					</div>
				))}
			</div>
		);
	}

	fetchScores = () => {
		fetch("/score/")
			.then(response => response.json())
			.then(data => {
				var sortedKeys = Object.keys(data).sort(
					(a, b) => parseFloat(data[a]) - parseFloat(data[b])
				);

				this.setState({
					scores: sortedKeys.map(key => {
						return {
							name: key,
							score: new Date(parseFloat(data[key]) * 1000)
								.toISOString()
								.substr(11, 8)
						};
					}),
					loading: false
				});
			})
			.catch(reason => {
				this.setState({
					loading: false,
					error: reason
				});
			});
	};
}
