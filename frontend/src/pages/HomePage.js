import React from "react";
import {
	Paper,
	Typography,
	Avatar,
	Grid,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Link
} from "@material-ui/core";
import { GitHub } from "@material-ui/icons";

export default class HomePage extends React.Component {
	render() {
		return (
			<>
				<div
					style={{
						textAlign: "center"
					}}
				>
					<div
						className="logo"
						style={{
							paddingTop: 28,
							fontSize: 60
						}}
					>
						Piball
					</div>

					<Typography variant="h6">
						A physics based game for the Raspberry Pi
					</Typography>

					<video
						style={{
							paddingTop: 40,
							height: 400
						}}
						autoPlay={true}
						loop
						controls
					>
						<source
							src="https://giant.gfycat.com/WhirlwindBlondFireant.webm"
							type="video/webm"
						></source>
					</video>
				</div>

				<Paper style={{ marginTop: 48, padding: "18px 22px" }}>
					<Grid container spacing={2}>
						<Grid item container spacing={2} alignItems="center">
							<Grid item xs={5}>
								<div
									style={{
										margin: 18
									}}
								>
									<img
										style={{
											width: "100%"
										}}
										src="/pi.png"
										alt="raspberry pi with touch screen"
									></img>
								</div>
							</Grid>
							<Grid item xs={7}>
								<Typography variant="h6">Tilt to Move</Typography>
								<Typography>
									PiBall runs on a Raspberry Pi equiped with a screen and an
									accelerometer.
								</Typography>
							</Grid>
						</Grid>
						<Grid item container spacing={2} alignItems="center">
							<Grid item xs={7}>
								<Typography variant="h6">Create New Levels</Typography>
								<Typography>
									New levels can be created using the{" "}
									<Link color="secondary" href="https://www.mapeditor.org/">
										Tiled Editor
									</Link>{" "}
									with custom made geometry editing tools. Import assets from
									the game and press the play button to instantly test your
									level in the engine.
								</Typography>
							</Grid>
							<Grid item xs={5}>
								<div
									style={{
										margin: 18
									}}
								>
									<img
										style={{
											width: "100%"
										}}
										src="/tiled.png"
										alt="Tiled Editor editing a level"
									></img>
								</div>
							</Grid>
						</Grid>
						<Grid item container direction="column" alignItems="center">
							<Typography
								variant="h5"
								style={{
									padding: "24px 0 24px 0"
								}}
							>
								The Team
							</Typography>
							<Grid item container direction="column" alignItems="center">
								<List>
									{[
										{
											name: "Juan Bofill",
											icon: "/juan.png",
											github: "jbofill10"
										},
										{
											name: "Jacob Coughenour",
											icon: "/jacob.png",
											github: "jacobcoughenour"
										}
									].map((e, key) => (
										<ListItem key={key}>
											<ListItemAvatar
												style={{
													paddingRight: 20
												}}
											>
												<Avatar
													src={e.icon}
													alt={e.name}
													style={{
														width: 80,
														height: 80
													}}
												></Avatar>
											</ListItemAvatar>
											<ListItemText
												primary={e.name}
												secondary={
													<Link
														href={"https://github.com/" + e.github}
														align="center"
														style={{
															display: "inline-flex",
															paddingTop: 4,
															alignItems: "center"
														}}
													>
														<GitHub color="secondary" fontSize="small" />
														<Typography
															component="span"
															style={{
																display: "inline",
																paddingLeft: 8
															}}
															color="textSecondary"
														>
															{e.github}
														</Typography>
													</Link>
												}
											></ListItemText>
										</ListItem>
									))}
								</List>
							</Grid>
						</Grid>
						<Grid></Grid>
					</Grid>
				</Paper>
			</>
		);
	}
}
