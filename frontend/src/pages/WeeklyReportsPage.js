import React from "react";
import { Typography, Paper } from "@material-ui/core";
import Accordion from "../components/FileAccordion";

export default class WeeklyReportsPage extends React.Component {
	render() {
		return (
			<>
				<div
					style={{
						textAlign: "center"
					}}
				>
					<Typography
						variant="h4"
						style={{
							paddingTop: 28
						}}
					>
						Weekly Reports
					</Typography>
				</div>

				<Paper style={{ marginTop: 48 }}>
					<Accordion
						week={"1"}
						tasks={
							<ul>
								<li>Installed Raspbian</li>
								<li>Had basic readings of the gyrosensor</li>
								<li>
									Downloaded all the necessary modules and libraries for the
									sensor and game
								</li>
								<li>Had a basic rendered object response to the gyrosensor</li>
								<li>
									Looked and found a program that could create levels for our
									game (Tiled)
								</li>
							</ul>
						}
						unfinished={
							<ul>
								<li>Get the Physics engine working</li>
							</ul>
						}
					/>

					<Accordion
						week={"2"}
						tasks={
							<ul>
								<li>Made good progress on the physics engine</li>
								<li>Learned how to use the map editor to make levels</li>
							</ul>
						}
						unfinished={
							<ul>
								<li>Get the Physics engine working</li>
							</ul>
						}
					/>

					<Accordion
						week={"3"}
						tasks={
							<ul>
								<li>
									Introduced basic game logic such as loading all the maps on
									start up
								</li>
								<li>Physics engine is now working</li>
							</ul>
						}
						unfinished={
							<ul>
								<li>
									Figuring out how to make the hitbox of the circle not
									rectangular
								</li>
								<li>
									Get the hole entity to detect that the ball crosses above it
									to "complete" the mission
								</li>
							</ul>
						}
					/>

					<Accordion
						week={"4"}
						tasks={
							<ul>
								<li>The hole now detects the ball when above it</li>
								<li>
									Added more game logic such as changing the rendered level when
									the player gets the ball in the hole
								</li>
							</ul>
						}
						unfinished={
							<ul>
								<li>Starting the website</li>
								<li>Add a main menu and GUI for the game</li>
							</ul>
						}
					/>

					<Accordion
						week={"5"}
						tasks={
							<ul>
								<li>Finished the main menu and GUI</li>
								<li>Made a few levels, nothing complicated though</li>
								<li>Set up all the libraries for the website</li>
							</ul>
						}
						unfinished={
							<ul>
								<li>Actually complete the website</li>
							</ul>
						}
					/>

					<Accordion
						week={"6"}
						tasks={
							<ul>
								<li>Finished the website</li>
								<li>
									Set up API routes between the frontend and backend to update
									high scores
								</li>
								<li>
									Added some fun obstacles to the game, like spikes, that cause
									you to start over from the start if contact is made
								</li>
							</ul>
						}
						unfinished={
							<ul>
								<li>We never did the Home Page or Weekly Report</li>
							</ul>
						}
					/>
				</Paper>
			</>
		);
	}
}
