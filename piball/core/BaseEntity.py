import math
from abc import ABC, abstractmethod

import inspect

from pygame import Rect
from pygame import SRCALPHA
from pygame import transform
from pygame.sprite import AbstractGroup
from pygame.sprite import Sprite
from pygame.surface import Surface
from PyTMX.pytmx import (TiledObject)


class BaseEntity(ABC, AbstractGroup):
    """Base class for game entity objects

    extends pygame.group
    """

    _subclasses = []
    """internal static list of the loaded classes that extend BaseEntity"""

    @staticmethod
    @abstractmethod
    def editor_color() -> str:
        """
        The color used to represent this entity type in the Tiled Editor.

        :return: a str hex color ("#a0a0a4")
        """
        return "#a0a0a4"

    @staticmethod
    @abstractmethod
    def default_props() -> dict:
        """
        Default properties for your entity to initialize with.

        These are the custom properties that are reflected in the Tiled Editor.

        BaseEntity handles the inheritance of these props for subclasses.
        So when you extend an Entity, you are also inheriting all of its default
        props even if you override default_props().

        Override to declare new default properties for your entity type.

        :return: dictionary of properties and their default values
        """
        return {}

    def __init__(self, tmx_object: TiledObject):
        """
        This is called by PiballMap when it loads the entity

        :param tmx_object: parsed tmx object for this entity
        """

        # init our abstract group base
        super().__init__()

        self.tmx_object = tmx_object
        """tmx object loaded from the map tmx file for this entity"""

        self.parent = tmx_object.parent
        """tmx parent"""

        # get the parent map
        cur_map = tmx_object.parent
        while hasattr(cur_map, "parent"):
            cur_map = cur_map.parent

        self.map = cur_map
        """PiballMap this entity belongs to"""

        self.b2world = self.map._b2world
        """Box2D world instance"""

        self.name = tmx_object.name
        """name given to this entity from the editor tmx"""
        self.id = tmx_object.id
        """tmx object id"""
        self.gid = tmx_object.gid
        """tmx object gid"""

        self.rect = Rect(tmx_object.x, tmx_object.y, tmx_object.width,
                         tmx_object.height)
        """entity bounding box (x, y, width, height)"""

        self.rotation = tmx_object.rotation
        """entity rotation
            
            Use this to rotate the whole entity
        """
        self.visible = tmx_object.visible
        """entity visibility"""

        self.image = tmx_object.image
        """tmx object image reference"""

        # The editor only save entity props that != their default value.
        # So we have to grab all the default prop values and apply the changed
        # ones from the tmx on-top of them.

        # combine all default props from base classes
        props = {}
        for cls in reversed(inspect.getmro(self.__class__)):
            if callable(getattr(cls, "default_props", None)):
                props.update(cls.default_props())

        # override our default props with values from the tmx object
        props.update(tmx_object.properties)

        self.properties = props
        """entity properties from default_props and tmx_object.properties"""

        self.default_sprite = Sprite()
        """default sprite to draw on
        
            This sprite is already added to this entity.
        
            This should be enough for most entities that don't change their geometry 
            and don't need several moving sub-sprites
            but for more control you should override the draw()
        """
        self.default_sprite.image = Surface((self.rect.w, self.rect.h), flags=SRCALPHA)
        """default sprite surface that gets rendered"""

        # match the default sprite rect to the entity's rect
        self.default_sprite.rect = self.rect

        self.sprite_offset = (0, 0)
        """offset the render position of all member sprites"""

        # add default sprite as a member to this group
        self.add(self.default_sprite)

    def __init_subclass__(cls, **kwargs):
        """
        subclass init

        do not override"""
        super().__init_subclass__(**kwargs)

        # append subclass to parent _subclasses list
        cls._subclasses.append(cls)

    def __post_load__(self):
        """
        Called after all of the map entities have been initialized and before update or draw.

        :return: None
        """
        pass

    def destroy(self):
        """
        Called before entity is remove from parent.

        :return: None
        """
        pass

    def update(self, delta):
        """
        Called every frame before draw().

        This should be used to position and rotation of your entity before it is drawn.

        Don't forget to call super().update(delta) to update all member entities

        :param delta: previous frame time in milliseconds
        :return: None
        """
        super(BaseEntity, self).update(delta)

    def draw(self, surface):
        """
        Called every frame to draw this entity to the parent surface

        Draws all of the member sprites onto the given surface.

        :param surface: pygame.surface.Surface surface to draw to (usually the screen)
        :return: None
        """

        # get all the member sprites for this group
        sprites = self.sprites()

        # grab the draw method from the surface
        surface_blit = surface.blit

        # for each of our member sprites
        for spr in sprites:
            # The Tiled editor rotates objects around their top-left corner and
            # pygame rotates from the center.  So we have to do some math to have
            # object rotation match what is in the editor.

            # the center position before applying the rotation
            old_center = (
                spr.rect.center[0] + self.sprite_offset[0],
                spr.rect.center[1] + self.sprite_offset[1]
            )

            # rotate the sprite image by the entity rotation
            rot_surf = transform.rotate(spr.image, -self.rotation)

            # get our new rect after the rotation
            rot_rect = rot_surf.get_rect()

            # move the new rotated rect position back to the starting position
            rot_rect.center = old_center

            # rotate center around top-left of sprite
            rot_rect.center = BaseEntity.rotate_point(
                (
                    spr.rect.center[0] + self.sprite_offset[0],
                    spr.rect.center[1] + self.sprite_offset[1]
                ),
                spr.rect.topleft,
                self.rotation
            )

            # draw to surface and store into group spritedict
            self.spritedict[spr] = surface_blit(
                rot_surf,
                rot_rect
            )

            # clear lost sprite from group
            self.lostsprites = []

    @staticmethod
    def rotate_point(point: tuple, origin: tuple, angle: float) -> object:
        """
        Rotates a 2D point around origin
        :param point: point to rotate
        :param origin: origin point to rotate around
        :param angle: how far to rotate in degrees
        :return: new position for the rotated point
        """
        sin_t = math.sin(math.radians(angle))
        cos_t = math.cos(math.radians(angle))

        return (origin[0] + (cos_t * (point[0] - origin[0]) - sin_t * (point[1] - origin[1])),
                origin[1] + (sin_t * (point[0] - origin[0]) + cos_t * (point[1] - origin[1])))
