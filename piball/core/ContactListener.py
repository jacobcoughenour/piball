from Box2D import b2ContactListener
from piball.core.PiballMapCanvas import PiballMapCanvas


class ContactListener(b2ContactListener):

    def __init__(self, game_map: PiballMapCanvas):
        b2ContactListener.__init__(self)
        self._game_map = game_map

    def BeginContact(self, contact):
        a = contact.fixtureA.userData["parent_entity"]
        b = contact.fixtureB.userData["parent_entity"]

        a.handle_begin_contact(contact, b)
        b.handle_begin_contact(contact, a)

    def EndContact(self, contact):
        a = contact.fixtureA.userData["parent_entity"]
        b = contact.fixtureB.userData["parent_entity"]

        a.handle_end_contact(contact, b)
        b.handle_end_contact(contact, a)

    def PreSolve(self, contact, oldManifold):
        pass

    def PostSolve(self, contact, impulse):
        pass
