import enum


class GameState(enum.Enum):
    Loading = 1
    MainMenu = 2
    Playing = 3
    Paused = 4
    Complete = 5
    Exiting = 6
