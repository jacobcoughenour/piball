import pygame
import math
from piball.core.constants import Constants

# center of the screen
center = (Constants.screen_width * 0.5, Constants.screen_height * 0.5)


class InputManager:
    """
    Input Event Manager.

    Only one of these should exist at a time.

    Handles mouse/touch input and sensor input.
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize the InputManager
        """
        super().__init__(*args, **kwargs)

        try:
            # try importing the sensor module
            from mpu6050 import mpu6050

            # connect to the module at address 0x68
            self.sensor = mpu6050(0x68)

            # successfully connected, so we don't need to use mouse controls
            self.use_mouse = False
        except:

            # failed to connect to the sensor
            print("failed to connect to sensor")
            print("falling back on mouse input")

            # use mouse controls
            self.use_mouse = True

    def get_tilt(self):
        """
        Gets the current tilt data from the sensor (or mouse).
        :return: (x, y) tilt from -1 to 1 with 0 being the center
        """

        # if we are using the mouse
        if self.use_mouse:
            # get the current mouse position
            mouse_pos = tuple(pygame.mouse.get_pos())
            # return a normalized mouse position as fake sensor data
            return (
                (mouse_pos[0] - center[0]) / center[0],
                (mouse_pos[1] - center[1]) / center[1]
            )

        # get the accel data from the sensor
        data = self.sensor.get_accel_data()

        # return the sensor data
        return (
            -data['y'] * 0.5,
            data['x'] * 0.5
        )
