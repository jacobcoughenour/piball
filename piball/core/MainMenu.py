from pygame import (SRCALPHA, BLEND_RGBA_ADD, font, draw,
                    MOUSEBUTTONUP, KEYDOWN)
from pygame.sprite import AbstractGroup
from pygame.surface import Surface

from piball.core.constants import Constants


class MainMenu(AbstractGroup):

    def __init__(self, start_game_callback):
        AbstractGroup.__init__(self)

        self._start_game_callback = start_game_callback

        # Sets time font
        self._title_font = font.Font(Constants.font, 32)
        self._info_font = font.Font(Constants.font, 16)

        self._title_text_pos = (Constants.screen_width // 2, Constants.screen_height // 3)
        self._info_text_pos = (Constants.screen_width // 2, Constants.screen_height // 3 * 2)

    def draw(self, surface):

        title_text = self._title_font.render(
            "PiBall",
            True,
            Constants.colors['white']
        )

        title_text_rect = title_text.get_rect()

        title_text_rect.center = self._title_text_pos

        surface.blit(title_text, title_text_rect)

        info_text = self._info_font.render(
            "press the screen to start",
            True,
            (200, 200, 200)
        )

        info_text_rect = info_text.get_rect()

        info_text_rect.center = self._info_text_pos

        surface.blit(info_text, info_text_rect)

    def handle_event(self, event):
        if event.type == MOUSEBUTTONUP or event.type == KEYDOWN:
            self._start_game()

    def _start_game(self):
        if self._start_game_callback:
            self._start_game_callback()
