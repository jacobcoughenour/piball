import importlib
import time

from Box2D import (b2World)
from datetime import timedelta
from pygame import (SRCALPHA, BLEND_RGBA_ADD, font)
from pygame.sprite import AbstractGroup
from pygame.surface import Surface
from PyTMX.pytmx import (TiledMap)

from piball.entities.WallEntity import WallEntity
from piball.core.constants import Constants

loaded_entity_classes = {}


class PiballMapCanvas(AbstractGroup, TiledMap):
    """
    Map loading and rendering manager.

    Acts as a container to load a map into.
    """
    _entities = {}
    """internal map entities dict"""

    _fg_entities = {}
    _bg_entities = {}
    _wall_entities = {}

    _current_playlist = list()
    _current_playlist_index = 0

    def __init__(self):
        AbstractGroup.__init__(self)

        self._b2world = None
        self._completion_callback = None
        self._level_callback = None
        self._playlist_start_time = None
        self._show_timer = False

    def start_playlist(self, paths: list, b2world: b2World, complete):
        """
        loads a tmx map after the previous is completed
        :param paths: paths to the tmx files
        :param b2world: physics world to add physics bodies to
        :param complete: callback for when the playlist is completed with time score
        :return: None
        """

        self._playlist_start_time = time.time()

        # Sets time font
        self._time_font = font.Font(Constants.font, 26)

        # set the center of the rectangular object.
        self._text_pos = (Constants.screen_width // 2, Constants.screen_height - 15)

        self._show_timer = True

        self._current_playlist_index = 0
        self._current_playlist = paths

        self._b2world = b2world
        self._completion_callback = complete

        # load the first one
        self.load(
            self._current_playlist[0],
            self._b2world,
            self._load_next
        )

    def _load_next(self):
        """
        Load the next level in the current playlist.
        If the current level is the last in the playlist, _playlist_complete is called
        :return:
        """
        self._current_playlist_index += 1

        if self._current_playlist_index >= len(self._current_playlist):
            self._playlist_complete()
            return

        self.load(
            self._current_playlist[self._current_playlist_index],
            self._b2world,
            self._load_next
        )

    def level_complete(self):
        """
        Called by the HoleEntity when a level is complete
        :return:
        """
        # unload the level
        self.unload()

        # call the level callback from load() if it exists
        if self._level_callback:
            self._level_callback()

    def _playlist_complete(self):
        # unload the current level
        self.unload()

        # hide the timer
        self._show_timer = False

        # if there is a playlist complete callback
        if self._completion_callback:
            # send the elapsed time to the callback
            self._completion_callback(time.time() - self._playlist_start_time)

    def load(self, path: str, b2world: b2World, complete):
        """
        Loads a tmx map file into the canvas
        :param path: path to the tmx file
        :param b2world: physics world to add physics bodies to
        :param complete: callback function called when the level is completed
        :return: None
        """

        # unload the current the map first
        self.unload()

        # let pytmx load the file and parse the tmx file
        TiledMap.__init__(self, path)

        # set our reference to the physics world
        self._b2world = b2world

        self._level_callback = complete

        # set the map background color
        if self.background_color:
            # convert bg color from hex to rgba
            self.bg_color = (
                int(self.background_color[-6:-4], 16),
                int(self.background_color[-4:-2], 16),
                int(self.background_color[-2:], 16),
                255 if len(self.background_color) == 7
                else int(self.background_color[1:3], 16)
            )

            # create a surface to draw the map to
            self.bg_surface = Surface((self.width * self.tilewidth, self.height * self.tileheight), flags=SRCALPHA)
            self.bg_surface.fill(self.bg_color)

        # for all the tmx object groups in the file

        for tmx_layer in self.all_layers:

            # skip hidden layers
            if not tmx_layer.visible or str(tmx_layer.name).startswith("_"):
                continue

            if tmx_layer.name == "walls_output":
                # create instance of entity from tmx object
                entity = WallEntity(tmx_layer[0], tmx_layer)

                # add entity to our dict
                self._entities[tmx_layer[0].id] = entity

                self._wall_entities[tmx_layer[0].id] = entity

                continue

            for tmx_object in tmx_layer:

                # ignore tmx objects with out a type
                if tmx_object.type is None:
                    print("PiballMap Load: object #" + str(tmx_object.id) + " has no type.  skipping it")
                    continue

                # load entity type class if not already loaded
                if tmx_object.type not in loaded_entity_classes:

                    # try loading the entity class my its file name
                    try:
                        module = importlib.import_module("piball.entities." + tmx_object.type)
                        loaded_entity_classes[tmx_object.type] = getattr(module, tmx_object.type)
                    except BaseException as e:
                        # print out the error when loading the entity
                        print(
                            "PiballMap Load: failed to load type \"" + tmx_object.type + "\" for object #" + str(
                                tmx_object.id))
                        print(e.__doc__)
                        print(e.message)
                        continue

                # create instance of entity from tmx object
                entity = loaded_entity_classes[tmx_object.type](tmx_object)

                # add entity to our dict
                self._entities[tmx_object.id] = entity

                if str(tmx_layer.name).startswith("bg"):
                    self._bg_entities[tmx_object.id] = entity
                else:
                    self._fg_entities[tmx_object.id] = entity

        entities = self._entities.items()

        # for all the loaded entities
        for id, loaded_entity in entities:
            # call the post_load event
            loaded_entity.__post_load__()

    def unload(self):
        """
        Unloads the current map
        :return: None
        """
        entities = self._entities.items()

        # call the destroy event for all the loaded entities
        for id, entity in entities:
            entity.destroy()

        # clear entity dict
        self._entities = {}
        self._fg_entities = {}
        self._bg_entities = {}
        self._wall_entities = {}

        self.bg_color = None
        self.bg_surface = None

    def update(self, delta):
        """
        Updates all the entities in the map.

        This should be called before draw().
        :param delta: previous frame time in milliseconds
        :return: None
        """
        entities = self._entities.items()

        for id, entity in entities:
            entity.update(delta=delta)

    def draw(self, surface):
        """
        Draw the map canvas and all of its entities
        :param surface: surface to draw to
        :return: None
        """

        # if the current map has a background color set
        if self.bg_color and self.bg_color[3] > 0:
            if self.bg_color[3] == 255:
                surface.fill(self.bg_color)
            else:
                surface.blit(self.bg_surface, (0, 0))

        # draw entities to the surface
        self._draw_entities(surface)

        # draw timer
        if self._show_timer:
            self._draw_time(surface)

    def _draw_entities(self, surface):
        """
        Draw all the entities to the surface directly
        :param surface: surface to draw to
        :return: None
        """
        fg_entities = self._fg_entities.items()
        bg_entities = self._bg_entities.items()
        wall_entities = self._wall_entities.items()

        # draw all the entities to the surface
        for id, entity in bg_entities:
            entity.draw(surface)
        for id, entity in fg_entities:
            entity.draw(surface)
        for id, entity in wall_entities:
            entity.draw(surface)

    def _draw_time(self, surface):

        # Gets current seconds and formats it
        text = self._time_font.render(
            str(
                "{:0>8}".format(str(
                    timedelta(
                        seconds=round(time.time() - self._playlist_start_time)
                    ))
                )
            ),
            True,
            Constants.colors['white']
        )

        text_rect = text.get_rect()

        # set the center of the rectangular object.
        text_rect.center = self._text_pos

        # Renders time
        surface.blit(text, text_rect)
