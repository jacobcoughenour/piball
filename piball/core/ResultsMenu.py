from datetime import timedelta
from pygame import (SRCALPHA, BLEND_RGBA_ADD, draw, key, font,
                    MOUSEBUTTONUP, KEYDOWN, K_BACKSPACE, K_RETURN)
from pygame.sprite import AbstractGroup
from pygame.surface import Surface

from piball.core.constants import Constants


class ResultsMenu(AbstractGroup):

    def __init__(self, restart_game_callback):
        AbstractGroup.__init__(self)

        self._restart_game_callback = restart_game_callback

        self._time_font = font.Font(Constants.font, 32)
        self._info_font = font.Font(Constants.font, 16)
        self._name_font = font.Font(Constants.font, 16)
        self._restart_font = font.Font(Constants.font, 16)

        self._time_text_pos = (Constants.screen_width // 2, Constants.screen_height // 3)
        self._info_text_pos = (Constants.screen_width // 2, Constants.screen_height // 2)
        self._name_text_pos = (Constants.screen_width // 2, Constants.screen_height // 2 + 20)
        self._restart_text_pos = (Constants.screen_width // 2, Constants.screen_height - 32)

        self.time = 0
        self.username = ''

    def set_time(self, time):
        self.time = time

    def draw(self, surface):
        time_text = self._time_font.render(
            "your time: " +
            str(self.time),
            True,
            (255, 255, 255)
        )
        time_text_rect = time_text.get_rect()
        time_text_rect.center = self._time_text_pos
        surface.blit(time_text, time_text_rect)

        info_text = self._info_font.render(
            "enter your name for the scoreboard:",
            True,
            (255, 255, 255)
        )
        info_text_rect = info_text.get_rect()
        info_text_rect.center = self._info_text_pos
        surface.blit(info_text, info_text_rect)

        name_text = self._name_font.render(
            str(self.username),
            True,
            (255, 255, 255)
        )
        name_text_rect = name_text.get_rect()
        name_text_rect.center = self._name_text_pos
        surface.blit(name_text, name_text_rect)

        if len(self.username) > 2:
            restart_text = self._restart_font.render(
                "press enter to submit your score",
                True,
                (200, 200, 200)
            )
            restart_text_rect = restart_text.get_rect()
            restart_text_rect.center = self._restart_text_pos
            surface.blit(restart_text, restart_text_rect)

    def handle_event(self, event):
        if event.type == KEYDOWN:
            if str(event.unicode).isalnum() and len(self.username) < 32:
                self.username += str(event.unicode).upper()
            elif event.key == K_BACKSPACE and len(self.username) > 0:
                self.username = self.username[:-1]
            elif len(self.username) > 2 and event.key == K_RETURN:
                self._restart_game()

    def _restart_game(self):
        if self._restart_game_callback:
            self._restart_game_callback(self.username, self.time)
