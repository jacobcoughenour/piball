from piball.core.GameState import GameState


class Constants:
    # global game state
    game_state = GameState.Loading

    # Used for game screen dimensions
    screen_width = 640
    screen_height = 480

    # physics settings
    physics_timestep = 1.0 / 60
    physics_vel_iters, physics_pos_iters = 16, 20

    # how many pixels 1 physics world unit represents in pygame
    physics_world_scale = 16
    physics_gravity_scale = 100

    fps = 60
    # Colors dict: we can just store all the colors we use here
    colors = {'black': (0, 0, 0), 'white': (255, 255, 255)}

    # default font face
    font = 'freesansbold.ttf'

    # main menu level
    main_menu_level = "piball/levels/main_menu.tmx"

    # intro playlist levels
    intro_levels = [
        "piball/levels/intro_0.tmx",
        "piball/levels/intro_1.tmx",
        "piball/levels/intro_2.tmx",
        "piball/levels/intro_3.tmx",
        # "piball/levels/intro_4.tmx",
    ]

    # time shit
    hour_min_sec = [3600, 60, 1]
