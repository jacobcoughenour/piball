"""

This script grabs all the entity classes (from piball/entities/__init__.py: __all__)
and exports them to objecttypes.xml for the Tiled Editor to read.

Run this script with `python -m piball.editor.gentypes`

In the Tiled Editor go to View > Object Types Editor > File > Choose Object Types File...
and select the objecttypes.xml file that was generated from this script.

"""

import importlib
import os
import string
import xml.etree.cElementTree as ET

from piball.core.BaseEntity import BaseEntity

# import all in entities so that they register to BaseEntity._subclasses

entities_module = importlib.import_module("piball.entities")
all_entities = getattr(entities_module, "__all__")

# dynamically import all the piball entity classes
for x in all_entities:
    importlib.import_module("piball.entities." + x)

print()
print("Generating object types for the tiled editor...")
print()

# python types and their matching tmx data types
types = {"int": "int", "float": "float", "str": "string", "bool": "bool"}

root = ET.Element("objecttypes")


def get_default_props(cls):
    """
    combine all the default props from inherited classes
    """

    if cls is BaseEntity:
        return {}
    result = get_default_props(cls.__base__)
    result.update(cls.default_props())
    return result


# for all the Entity classes that extend BaseEntity
for x in BaseEntity._subclasses:

    # skip classes that are not in the __all__ list
    if x.__name__ not in all_entities:
        continue

    print("type", x.__name__ + ":")
    print("\teditor_color: " + x.editor_color())

    objecttype = ET.SubElement(
        root, "objecttype", name=x.__name__, color=x.editor_color())

    print("\tdefault_props:")

    for key, val in get_default_props(x).items():

        print("\t\t\"" + key + "\":", val)

        typestr = str(type(val))[8:-2]

        # if the string is a color
        if typestr == "str" and len(val) == 9 and val[0] == "#" and all(
                c in string.hexdigits for c in val[1:9]):
            typestr = "color"
        else:
            typestr = types[typestr] if typestr in types else "string"

        value = str(val)

        ET.SubElement(objecttype, "property", {
            "name": key,
            "type": typestr,
            "default": value.lower() if typestr == "boolean" else value
        })
    print()

print()
print("Writing to objecttypes.xml...")

ET.ElementTree(root).write(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "objecttypes.xml"))

print("Complete!")
