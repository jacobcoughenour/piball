const path = require("path");
const gulp = require("gulp");
const compiler = require("webpack");
const webpack = require("webpack-stream");

// path to the source code
const sourcePath = path.resolve(__dirname, "./src");
// path for webpack output
const outputPath = path.resolve(__dirname, "./dist");

// path to the tiled extensions directory
const tiledExtensionsPath =
	// https://doc.mapeditor.org/en/latest/reference/scripting/#scripted-extensions

	process.platform === "win32"
		? path.join(process.env.HOME, "/AppData/Local/Tiled/extensions")
		: process.platform === "darwin"
		? path.join(process.env.HOME, "/Library/Preferences/Tiled/extensions")
		: path.join(process.env.HOME, "/.config/tiled/extensions");

const webpackConfig = {
	entry: path.join(sourcePath, "index.js"),
	mode: "production",
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-env"]
					}
				}
			}
		]
	},
	output: {
		filename: "main.js"
	},
	target: "web",
	externals: ["tiled"]
};

const build = () =>
	gulp
		.src(sourcePath + "/**/*.js")
		.pipe(
			webpack(webpackConfig, compiler, (err, stats) => {
				if (err) console.error(err);
			})
		)
		.pipe(gulp.dest(outputPath));

const installExtension = () =>
	gulp.src("./dist/**/*.js").pipe(gulp.dest(tiledExtensionsPath));

const watch = gulp.series(
	cb => {
		console.log("start watching");
		cb();
	},
	build,
	installExtension,
	() =>
		gulp.watch(
			"./src/**/*.*",
			gulp.series(
				cb => {
					console.log("restarting");
					cb();
				},
				build,
				installExtension
			)
		)
);

exports.watch = watch;
exports.build = build;
exports.installExtension = installExtension;
exports.default = build;
