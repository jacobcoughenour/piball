import { unify, subtract as _subtract } from "@flatten-js/boolean-op";
import { Polygon, Face, point, ORIENTATION } from "@flatten-js/core";

const circleResolution = 1 / 64;

const splitting = true;

tiled.assetAboutToBeSaved.connect(function(asset) {
	processLayers(asset);
});

console.log(Date.now());

function processLayers(asset) {
	for (let i = 0; i < asset.layerCount; i++) {
		var layer = asset.layerAt(i);

		if (layer.isGroupLayer) {
			if (layer.name.startsWith("_walls")) {
				processWallsLayerGroup(asset, layer);
			} else {
				processLayers(layer);
			}
		}
	}
}

function processWallsLayerGroup(asset, layer) {
	if (!layer.isGroupLayer) return;

	var finalPoly = new Polygon();

	for (let i = 0; i < layer.layerCount; i++) {
		var subLayer = layer.layerAt(i);

		if (
			!subLayer.isObjectLayer ||
			!subLayer.name.startsWith("_") ||
			!subLayer.visible
		)
			continue;

		var unifiedLayerPoly = new Polygon();

		for (let j = 0; j < subLayer.objectCount; j++) {
			var object = subLayer.objectAt(j);
			if (!object.visible) continue;

			var objectPoly = getPolygon(object).rotate(
				(object.rotation / 180) * Math.PI,
				point(object.x, object.y)
			);

			unifiedLayerPoly = unify(unifiedLayerPoly, objectPoly);
		}

		if (subLayer.name.startsWith("add", 1)) {
			subLayer.color = "#AA00FF00";

			finalPoly = unify(finalPoly, unifiedLayerPoly);
		} else if (subLayer.name.startsWith("sub", 1)) {
			subLayer.color = "#AAFF9C16";

			finalPoly = subtract(finalPoly, unifiedLayerPoly);
		}
	}

	// remove previous output layer(s)
	var layerCount = asset.layerCount;
	for (let i = 0; i < layerCount; i++) {
		var subLayer = asset.layerAt(i);
		if (subLayer.isObjectLayer && subLayer.name.startsWith("walls_output")) {
			asset.removeLayerAt(i);
			i--;
			layerCount = asset.layerCount;
		}
	}

	// make faces convex

	var convexFaces = [];
	var badFaces = [];

	for (var face of finalPoly.faces) {
		var points = [];

		for (var edge of face) {
			points.push(point(edge.shape.ps.x, edge.shape.ps.y));
		}

		if (splitting) {
			for (var splitFace of splitIntoConvexFaces(points)) {
				if (splitFace.convex) convexFaces.push(splitFace.points);
				else badFaces.push(splitFace.points);
			}
		} else convexFaces.push(points);
	}

	if (badFaces.length > 0) {
		var group = new ObjectGroup("walls_output_invalid");

		group.color = "#FFFF0000";

		for (var i = 0; i < badFaces.length; i++) {
			var object = new MapObject(1);

			var points = [];

			for (let p = 0; p < badFaces[i].length; p++) {
				points.push({ x: badFaces[i][p].x, y: badFaces[i][p].y });
			}

			object.polygon = points;

			group.addObject(object);
		}
		group.locked = true;

		asset.insertLayerAt(0, group);
	}

	if (convexFaces.length > 0) {
		var group = new ObjectGroup("walls_output");

		for (var i = 0; i < convexFaces.length; i++) {
			var object = new MapObject(1);

			var points = [];

			for (let p = 0; p < convexFaces[i].length; p++) {
				points.push({
					x: convexFaces[i][p].x,
					y: convexFaces[i][p].y
				});
			}

			object.polygon = points;

			object.type = "StaticPhysicsEntity";

			group.addObject(object);
		}
		group.locked = true;

		if (badFaces.length > 0) {
			group.opacity = 0.5;
		}

		asset.insertLayerAt(0, group);
	}
}

function splitIntoConvexFaces(points) {
	if (points.length < 3) return [{ points: [], convex: true }];

	if (points.length === 3) {
		// if counter clockwise, flip it
		if (
			orientation(
				points[0].x,
				points[0].y,
				points[1].x,
				points[1].y,
				points[2].x,
				points[2].y
			)
		) {
			return [
				{
					points: [points[0], points[2], points[1]],
					convex: true
				}
			];
		}
		return [
			{
				points: points,
				convex: true
			}
		];
	}

	var len = points.length;
	var isConcave = false;

	// for all points
	for (let i = 0; i < len; i++) {
		// get triangle
		var a = points[i];
		var bi = (i + 1) % len;
		var b = points[bi];
		var ci = (i + 2) % len;
		var c = points[ci];

		// tri is counter clockwise
		if (orientation(a.x, a.y, b.x, b.y, c.x, c.y) === 1) {
			// so the face is concave
			isConcave = true;

			// we try splitting it at b

			// find a valid line between b and some other point
			// we'll call this the slice line

			// start on the opposite side of the poly
			var opp = (i + 1 + Math.floor(len / 2)) % len;

			for (let j = 0, kbi = 0; j < len; j++) {
				// convert the linear index j to alternating index kbi
				// j => [0,1,2,3,4,5,...]
				// kbi => [0,1,-1,2,-2,3,...]

				if (j % 2 === 0) {
					// look behind
					kbi = (len + opp - j / 2) % len;
				} else {
					// look ahead
					kbi = (opp + (j + 1) / 2) % len;
				}

				// skip triangle abc
				// TODO setup the for condition to never hit abc
				if (kbi === i || kbi === bi || kbi === ci) continue;

				var splitFaces = trySplitFace(points, bi, kbi, true, 16);

				if (splitFaces) {
					if (splitFaces.length === 1)
						return splitIntoConvexFaces(splitFaces[0]);
					return splitIntoConvexFaces(splitFaces[0]).concat(
						splitIntoConvexFaces(splitFaces[1])
					);
				} else {
					// try another slice line
					continue;
				}
			}
		}
	}

	// split if over the box2d limit
	if (len > 16) {
		// for all points
		for (let i = 0; i < len; i++) {
			// get triangle
			var a = points[i];
			var bi = (i + 1) % len;
			var b = points[bi];
			var ci = (i + 2) % len;
			var c = points[ci];

			// we try splitting it at b

			// find a valid line between b and some other point
			// we'll call this the slice line

			// start on the opposite side of the poly
			var opp = (i + 1 + Math.floor(len / 2)) % len;

			for (let j = 0, kbi = 0; j < len; j++) {
				// convert the linear index j to alternating index kbi
				// j => [0,1,2,3,4,5,...]
				// kbi => [0,1,-1,2,-2,3,...]

				if (j % 2 === 0) {
					// look behind
					kbi = (len + opp - j / 2) % len;
				} else {
					// look ahead
					kbi = (opp + (j + 1) / 2) % len;
				}

				// skip triangle abc
				// TODO setup the for condition to never hit abc
				if (kbi === i || kbi === bi || kbi === ci) continue;

				var splitFaces = trySplitFace(points, bi, kbi, false, 16);

				if (splitFaces) {
					if (splitFaces.length === 1)
						return splitIntoConvexFaces(splitFaces[0]);
					return splitIntoConvexFaces(splitFaces[0]).concat(
						splitIntoConvexFaces(splitFaces[1])
					);
				} else {
					// try another slice line
					continue;
				}
			}
		}
	}

	return [
		{
			points: points,
			// if we found a concave triangle but couldn't find a line to slice
			convex: !isConcave
		}
	];
}

function trySplitFace(points, start, end, disolve = false, minArea = -1) {
	// if indices are the same
	if (start === end) return false;

	var len = points.length;

	// if out of bounds
	if (start < 0 || end < 0 || start >= len || end >= len) return false;

	// if indices are already connected
	if (
		Math.abs(start - end) === 1 ||
		(start === 0 && end === len - 1) ||
		(end === 0 && start === len - 1)
	)
		return false;

	// start triangle
	var a1i = (len + start - 1) % len,
		c1i = (start + 1) % len;

	// end triangle
	var a2i = (len + end - 1) % len,
		c2i = (end + 1) % len;

	// start point
	var b1 = points[start];

	// end point
	var b2 = points[end];

	// create our slice line
	var sliceLine = [b1.x, b1.y, b2.x, b2.y];

	// check for slice line against all other lines in the face
	// for intersections
	if (
		points.some((point, i) => {
			// skip slice points
			if (
				i === start ||
				i === end ||
				i === a1i ||
				i === c1i ||
				i === a2i ||
				i === c2i
			)
				return false;

			var nextpoint = points[(i + 1) % len];
			return (
				getIntersection(sliceLine, [point.x, point.y, nextpoint.x, nextpoint.y])
					.length !== 0
			);
		})
	) {
		// slice line intersects with a segment
		return false;
	}

	// start triangle
	var a1 = points[a1i],
		c1 = points[c1i];

	// end triangle
	var a2 = points[a2i],
		c2 = points[c2i];

	// check if the slice line is inside the face

	//
	// Valid slice line:
	//
	//     ------------
	//    : ++++++++++ :
	//   c2 ++++++++++ a1
	//    | ++++++++  /
	//   b2 -- sL -- b1
	//    | +++++++++ \
	//   a2 ++++++++++ c1
	//    : ++++++++++ :
	//     ------------

	//
	// Invalid slice line outside of the polygon
	//
	// +++++++++++++ :
	// ++++++++++++ /
	// ++++++ b1 - a1
	// +++++ /  \
	// ++++ c1   sL
	// +++++ \    \
	// ++++++ a2 - b2
	// ++++++++++++ \
	// +++++++++++++ c2
	// +++++++++++++ :

	// so we check the triangles

	if (
		// if triangle 1 is counter clockwise
		(orientation(a1.x, a1.y, b1.x, b1.y, c1.x, c1.y) === 1
			? // b2 is on the outside of line a1-b1
			  orientation(a1.x, a1.y, b1.x, b1.y, b2.x, b2.y) === 1 &&
			  // b2 is on the outside of line b1-c1
			  orientation(b1.x, b1.y, c1.x, c1.y, b2.x, b2.y) === 1
			: // b2 is on the outside of line a1-b1
			  orientation(a1.x, a1.y, b1.x, b1.y, b2.x, b2.y) === 1 ||
			  // b2 is on the outside of line b1-c1
			  orientation(b1.x, b1.y, c1.x, c1.y, b2.x, b2.y) === 1) ||
		// if triangle 2 is counter clockwise
		(orientation(a2.x, a2.y, b2.x, b2.y, b2.x, b2.y) === 1
			? // b1 is on the outside of line a2-b2
			  orientation(a2.x, a2.y, b2.x, b2.y, b1.x, b1.y) === 1 &&
			  // b1 is on the outside of line b2-c2
			  orientation(b2.x, b2.y, c2.x, c2.y, b1.x, b1.y) === 1
			: // b1 is on the outside of line a2-b2
			  orientation(a2.x, a2.y, b2.x, b2.y, b1.x, b1.y) === 1 ||
			  // b1 is on the outside of line b2-c2
			  orientation(b2.x, b2.y, c2.x, c2.y, b1.x, b1.y) === 1)
	)
		return false;

	// var rotThres = 0.0000001;

	// var sliceRotation = getRotation(b1, b2);

	// if (
	// 	Math.abs(getRotationDifference(sliceRotation, getRotation(b1, a1))) <
	// 	rotThres
	// ) {
	// 	start = (len + start - 1) % len;
	// 	console.log("hi");
	// }
	// if (
	// 	Math.abs(getRotationDifference(sliceRotation, getRotation(b1, c1))) <
	// 	rotThres
	// ) {
	// 	start = (start + 1) % len;
	// 	console.log("hi1");
	// }
	// if (
	// 	Math.abs(getRotationDifference(sliceRotation, getRotation(a2, b2))) <
	// 	rotThres
	// ) {
	// 	end = (end + 1) % len;

	// 	console.log("hi2");
	// }
	// if (
	// 	Math.abs(getRotationDifference(sliceRotation, getRotation(c2, b2))) <
	// 	rotThres
	// ) {
	// 	end = (len + end + 1) % len;
	// 	console.log("hi3");
	// }

	// the slice line is valid

	// slice the points along the line

	// get all the points from b to kb
	var faceApoints =
		end < start
			? points.slice(start).concat(points.slice(0, end + 1))
			: points.slice(start, end + 1);
	// get all the points from kb to b
	var faceBpoints =
		start < end
			? points.slice(end).concat(points.slice(0, start + 1))
			: points.slice(end, start + 1);

	if (minArea >= 0) {
		// check the area of the resulting faces
		if (new Polygon().addFace(faceApoints).area() < minArea)
			return disolve ? [faceBpoints] : false;

		if (new Polygon().addFace(faceBpoints).area() < minArea)
			return disolve ? [faceApoints] : false;
	}

	// split into two faces
	return [faceApoints, faceBpoints];
}

function getRotation(a, b) {
	return Math.atan2(a.x - b.x, b.y - a.y);
}

function getRotationDifference(a, b) {
	var aRot = a,
		bRot = b;

	var rotDiff = bRot - aRot;

	if (rotDiff === -Math.PI) return Math.PI;
	if (rotDiff < -Math.PI) return Math.PI + (rotDiff % Math.PI);
	if (rotDiff > Math.PI) return rotDiff - Math.PI * 2;

	return rotDiff;
}

function subtract(a, b) {
	let resultPoly = a.clone();

	for (var faceB of b.faces) {
		let nextPoly = new Polygon();

		for (var faceA of resultPoly.faces) {
			var resultFaces = subtractFace(faceA, faceB);
			for (var faceR of resultFaces) {
				nextPoly.addFace(faceR);
			}
		}

		resultPoly = nextPoly;
	}

	return resultPoly;
}

function subtractFace(a, b) {
	// bounding boxes do not intersect
	// do nothing to a

	if (a.box.not_intersect(b.box)) return [a];

	var ap = new Polygon();
	ap.addFace(a);

	var prevcontains = -1;

	// look for intersections
	for (var edgeB of b) {
		var contains = ap.contains(edgeB.shape) ? 1 : 0;
		if (prevcontains !== -1 && contains !== prevcontains) {
			// we found an intersection
			// let flatten handle this one

			var bp = new Polygon();
			bp.addFace(b);
			return _subtract(ap, bp).faces;
		}

		prevcontains = contains;

		var lineB = [
			edgeB.shape.ps.x,
			edgeB.shape.ps.y,
			edgeB.shape.pe.x,
			edgeB.shape.pe.y
		];

		for (var edgeA of a) {
			var inter = getIntersection(
				[
					edgeA.shape.ps.x,
					edgeA.shape.ps.y,
					edgeA.shape.pe.x,
					edgeA.shape.pe.y
				],
				lineB
			);

			if (inter.length > 0) {
				// we found an intersection
				// let flatten handle this one

				var bp = new Polygon();
				bp.addFace(b);
				return _subtract(ap, bp).faces || [];
			}
		}
	}

	// b is entirely contained within a
	if (prevcontains === 1) {
		if (a.orientation() === -1) {
			a.reverse();
		}
		if (b.orientation() === -1) {
			b.reverse();
		}

		var bp = new Polygon();
		bp.addFace(b);

		var [dist, line] = ap.distanceTo(bp);

		// we need to find the points of the line along the face edges

		var testLine = [line.ps.x, line.ps.y, line.pe.x, line.pe.y];

		var tolerance = 0.01;

		var averts = ap.vertices;
		var alen = averts.length;
		var bverts = bp.vertices.reverse();
		var blen = bverts.length;

		var aInters = [];

		for (let i = 0; i < alen; i++) {
			var inter = getIntersection(
				testLine,
				[
					averts[i].x,
					averts[i].y,
					averts[(i + 1) % alen].x,
					averts[(i + 1) % alen].y
				],
				tolerance
			);
			if (inter.length !== 0) {
				var p = point(inter[0], inter[1]);

				aInters.push({
					index: i,
					point: p
				});
				if (aInters.length > 1) break;
			}
		}

		var bInters = [];

		for (let j = 0; j < blen; j++) {
			var inter = getIntersection(
				testLine,
				[
					bverts[j].x,
					bverts[j].y,
					bverts[(j + 1) % blen].x,
					bverts[(j + 1) % blen].y
				],
				tolerance
			);
			if (inter.length !== 0) {
				var p = point(inter[0], inter[1]);

				bInters.push({
					index: j,
					point: p
				});
				if (bInters.length > 1) break;
			}
		}

		if (aInters.length === 1) {
			// insert slice line point into verts
			let x = averts.slice(0, aInters[0].index + 1);
			x.push(aInters[0].point);
			averts = x.concat(averts.slice(aInters[0].index + 1));
		}

		if (bInters.length === 1) {
			// insert slice line point into verts
			let x = bverts.slice(0, bInters[0].index + 1);
			x.push(bInters[0].point);
			bverts = x.concat(bverts.slice(bInters[0].index + 1));
		}

		var finalVerts = [];

		finalVerts = averts
			.slice(0, aInters[0].index + 1)
			.concat([aInters[0].point, bInters[0].point])
			.concat(
				bverts
					.slice(bInters[0].index + 1)
					.concat(bverts.slice(0, bInters[0].index + 1))
					.reverse()
			)
			.concat([point(aInters[0].point.x, aInters[0].point.y + tolerance)])
			.concat(averts.slice(aInters[0].index + 2));

		var final = new Polygon();

		// var split = splitIntoConvexFaces(finalVerts);

		// for (var v = 0; v < split.length; v++) {
		// 	final.addFace(split[v]);
		// }

		final.addFace(finalVerts);

		return final.faces;
	} else {
		return [];
	}
}

function getPolygon(object) {
	/**
	 * 0 rectangle
	 * 1 polygon
	 * 2 polyline
	 * 3 ellipse
	 * 4 text
	 * 5 point
	 */

	// rectangle
	if (object.shape === 0) {
		let x = object.x;
		let y = object.y;
		let w = x + object.width;
		let h = y + object.height;

		let polygon = new Polygon();
		// prettier-ignore
		polygon.addFace([
			// 0-1
			// | |
			// 3-2
			point(x, y),
			point(w, y),
			point(w, h),
			point(x, h)
		]);

		return polygon;
	}

	// polygon
	if (object.shape === 1) {
		let x = object.x;
		let y = object.y;
		var points = [];

		for (let i = 0; i < object.polygon.length; i++) {
			var vert = object.polygon[i];
			points.push(point(x + vert.x, y + vert.y));
		}
		var finalPoly = new Polygon();
		finalPoly.addFace(points);

		return finalPoly;
	}

	// polyline
	if (object.shape === 2) {
		let x = object.x;
		let y = object.y;

		var thickness = object.properties().thickness || 10;
		object.setProperty("thickness", thickness);

		var startPoint = {
			point: point(x + object.polygon[0].x, y + object.polygon[0].y),
			next: null,
			prev: null
		};

		var lastPoint = startPoint;

		for (let i = 1; i < object.polygon.length; i++) {
			var vert = object.polygon[i];
			lastPoint.next = {
				point: point(x + vert.x, y + vert.y),
				next: null,
				prev: lastPoint
			};

			lastPoint = lastPoint.next;
		}

		for (let i = object.polygon.length - 2; i > 0; i--) {
			var vert = object.polygon[i];
			lastPoint.next = {
				point: point(x + vert.x, y + vert.y),
				next: null,
				prev: lastPoint
			};

			lastPoint = lastPoint.next;
		}

		lastPoint.next = startPoint;
		startPoint.prev = lastPoint;

		var cur = startPoint;

		do {
			// get the rotation of the line segment from point to point.next
			cur.rotation = getRotation(cur.point, cur.next.point);
			// remove point if it matches the previous point rotation
			if (cur.rotation === cur.prev.rotation) {
				cur.prev.next = cur.next;
				cur.next.prev = cur.prev;
			}
			// go to next point
			cur = cur.next;
		} while (cur !== startPoint);

		cur = startPoint;
		var verts = [];

		do {
			var prevRotDiff = getRotationDifference(cur.prev.rotation, cur.rotation);

			if (prevRotDiff >= 0) {
				verts.push(
					cur.point.translate(thickness, 0).rotate(cur.rotation, cur.point)
				);
			}

			var rotDiff = getRotationDifference(cur.rotation, cur.next.rotation);

			if (rotDiff > 0) {
				var start = cur.next.point.translate(thickness, 0);

				var circumference = rotDiff * thickness;

				var segments = Math.max(
					Math.ceil(circumference * circleResolution * 4),
					1
				);

				var step = rotDiff / segments;

				for (let i = 0; i < segments; i++) {
					verts.push(start.rotate(cur.rotation + step * i, cur.next.point));
				}
			} else if (rotDiff < 0) {
				var b = cur.next.point
					.translate(thickness / Math.cos(-rotDiff / 2), 0)
					// .translate(thickness, 0)
					.rotate(cur.rotation + rotDiff / 2, cur.next.point);

				verts.push(b);
			}

			cur = cur.next;
		} while (cur !== startPoint);

		var finalPoly = new Polygon();

		finalPoly.addFace(verts);

		return finalPoly;
	}

	// ellipse
	if (object.shape === 3) {
		var points = [];

		let x = object.x;
		let y = object.y;
		var halfwidth = object.width * 0.5;
		var halfheight = object.height * 0.5;

		var circumference =
			2 *
			Math.PI *
			Math.sqrt(Math.pow(halfwidth, 2) + Math.pow(halfheight, 2) / 2);

		var segments = Math.max(Math.ceil(circumference * circleResolution), 2) * 4;

		var step = (Math.PI * 2) / segments;

		for (let i = 0; i < segments; i++) {
			points.push(
				point(
					x + halfwidth + Math.sin(step * i) * halfwidth,
					y + halfheight - Math.cos(step * i) * halfheight
				)
			);
		}

		let polygon = new Polygon();
		polygon.addFace(points);

		return polygon;
	}

	return new Polygon();
}

function applyTransformation(localVerticies, x, y, rotation) {
	// todo rotate first

	for (let i = 0; i < localVerticies.length; i++) {
		let pos = rotate(0, 0, localVerticies[i].X, localVerticies[i].Y, -rotation);

		localVerticies[i] = { X: pos[0] + x, Y: pos[1] + y };
	}

	return localVerticies;
}

function getIntersection(a, b, tolerance = 0) {
	var x1 = a[0];
	var x2 = a[2];
	var x3 = b[0];
	var x4 = b[2];

	var y1 = a[1];
	var y2 = a[3];
	var y3 = b[1];
	var y4 = b[3];

	var x12 = x1 - x2;
	var x34 = x3 - x4;

	var y12 = y1 - y2;
	var y34 = y3 - y4;

	var c = x12 * y34 - y12 * x34;

	if (Math.abs(c) < 0.0001) return [];

	var ap = x1 * y2 - y1 * x2;
	var bp = x3 * y4 - y3 * x4;

	var x = (ap * x34 - bp * x12) / c;
	var y = (ap * y34 - bp * y12) / c;

	// check if intersection is in bounding boxes
	if (
		x + tolerance >= Math.max(Math.min(x1, x2), Math.min(x3, x4)) &&
		y + tolerance >= Math.max(Math.min(y1, y2), Math.min(y3, y4)) &&
		x - tolerance <= Math.min(Math.max(x1, x2), Math.max(x3, x4)) &&
		y - tolerance <= Math.min(Math.max(y1, y2), Math.max(y3, y4))
	)
		return [x, y];

	return [];
}

function normalizePolygon(polygon) {
	for (var face of polygon.faces) {
		if (face.orientation() === -1) {
			face.reverse();
		}
	}
}

function rotate(cx, cy, x, y, rotation) {
	var radians = (Math.PI / 180) * rotation,
		cos = Math.cos(radians),
		sin = Math.sin(radians),
		nx = cos * (x - cx) + sin * (y - cy) + cx,
		ny = cos * (y - cy) - sin * (x - cx) + cy;
	return [nx, ny];
}

function orientation(ax, ay, bx, by, cx, cy) {
	var val = (by - ay) * (cx - bx) - (bx - ax) * (cy - by);
	if (val === 0 || EQ_0(val)) return 0;
	return val > 0 ? 1 : -1;
}

var DP_TOL = 0.000001;
function EQ_0(x) {
	return x < DP_TOL && x > -DP_TOL;
}

var createAction = tiled.registerAction("CreateGeometryGroupLayer", function(
	action
) {
	console.log(action);
});

createAction.text = "custom action here";

tiled.extendMenu("GroupLayer", [
	{ action: "CreateGeometryGroupLayer" },
	{ separator: true }
]);

function dump(object, depth = 1) {
	if (typeof object === "object") _dump(object, depth, 0);
	else console.log(object);
}

function _dump(object, depth, indent) {
	if (!object) return;

	var keys = Object.keys(object);

	for (let i = 0; i < keys.length; i++) {
		var prop = keys[i];
		var cur = object[prop];

		if (depth > 0 && typeof cur === "object") {
			console.log("  ".repeat(indent) + prop + ": " + typeof cur);
			_dump(cur, depth - 1, indent + 1);
		} else {
			console.log("  ".repeat(indent) + prop + ": " + cur);
		}
	}
}
