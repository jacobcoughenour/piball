from PyTMX.pytmx import (TiledObject)
from Box2D import b2Vec2

from piball.entities.PhysicsEntity import PhysicsEntity
from piball.core.constants import Constants


class DynamicPhysicsEntity(PhysicsEntity):
    """
    A basic physics entity affected by gravity and can collide with other physics entities.
    """

    @staticmethod
    def editor_color() -> str:
        return "#1DE9B6"

    @staticmethod
    def default_props() -> dict:
        return {
            "physics_density": 0.1,
            "physics_friction": 0.1,
            "color": "#ff1DE9B6"
        }

    def __init__(self, tmx_object: TiledObject):
        super().__init__(tmx_object)

        # if it's a polygon object

        if hasattr(self.tmx_object, "points"):

            rect_min = self.tmx_object.points[0]
            rect_max = rect_min

            verts = []

            for (x, y) in self.tmx_object.points:
                rect_min = (min(rect_min[0], x), min(rect_min[1], y))
                rect_max = (max(rect_max[0], x), max(rect_max[1], y))

                verts.append((x, y))

            center = (
                (rect_max[0] - rect_min[0]) * 0.5,
                (rect_max[1] - rect_min[1]) * 0.5
            )

            self.body = self.b2world.CreateDynamicBody(
                position=(
                    self.physics_rect[0] + center[0] / Constants.physics_world_scale,
                    self.physics_rect[1] + center[1] / Constants.physics_world_scale
                ),
                angle=self.physics_rotation
            )

            self.body.CreatePolygonFixture(
                vertices=list(
                    map(
                        lambda vert: (
                            (vert[0] - rect_min[0] - center[0] + self.sprite_offset[0]) / Constants.physics_world_scale,
                            (vert[1] - rect_min[1] - center[1] + self.sprite_offset[1]) / Constants.physics_world_scale
                        ),
                        verts
                    )
                ),
                density=self.properties["physics_density"],
                friction=self.properties["physics_friction"]
            )

        else:

            self.body = self.b2world.CreateDynamicBody(
                position=(
                    self.physics_rect[0] + (self.physics_rect[2] * 0.5),
                    self.physics_rect[1] + (self.physics_rect[3] * 0.5)
                ),
                angle=self.physics_rotation
            )

            # if it's a circle object

            if self.properties["circle"]:
                self.body.CreateCircleFixture(
                    radius=min(self.physics_rect[2], self.physics_rect[3]) * 0.5,
                    density=self.properties["physics_density"],
                    friction=self.properties["physics_friction"]
                )

            # if it's a rectangle object

            else:
                self.body.CreatePolygonFixture(
                    box=(self.physics_rect[2] * 0.5, self.physics_rect[3] * 0.5),
                    density=self.properties["physics_density"],
                    friction=self.properties["physics_friction"]
                )

        # do not allow the physics body to "sleep" when it stops moving.
        # this prevents it from getting stuck
        self.body.sleepingAllowed = False

    def teleport(self, x, y):
        """ Teleports body to new position and resets velocity"""
        self._next_physics_position = (x / Constants.physics_world_scale, y / Constants.physics_world_scale)
        self._next_physics_linearVelocity = (0, 0)
        self._next_physics_angularVelocity = 0
