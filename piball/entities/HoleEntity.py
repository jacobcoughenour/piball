from PyTMX.pytmx import (TiledObject)
from Box2D import (b2PolygonShape, b2CircleShape)

from piball.entities.PhysicsEntity import PhysicsEntity
from piball.entities.PlayerEntity import PlayerEntity
from piball.core.constants import Constants

import math


class HoleEntity(PhysicsEntity):
    """ Hole Entity

    Hole for the ball (PlayerEntity) to go in to complete the level
    """

    @staticmethod
    def editor_color() -> str:
        return "#212121"

    @staticmethod
    def default_props() -> dict:
        return {
            "circle": True,
            "physics_density": 0.1,
            "physics_friction": 0.1,
            "color": "#ff212121"
        }

    def __init__(self, tmx_object: TiledObject):
        super().__init__(tmx_object)

        # create a static physics body from tmx props
        self.body = self.b2world.CreateStaticBody(
            position=(
                self.physics_rect[0] + (self.physics_rect[2] * 0.5),
                self.physics_rect[1] + (self.physics_rect[3] * 0.5)
            ),
            angle=self.physics_rotation
        )

        # create physics body fixture from tmx props
        fixture = self.body.CreateCircleFixture(
            radius=(self.physics_rect[2] * 0.5),
            density=self.properties["physics_density"],
            friction=self.properties["physics_friction"]
        )

        # this allows other physics bodies to pass through the hole fixture
        # but the fixture will still receive collision events.
        fixture.sensor = True

        self._ball_near_hole = False
        self._player_ent = None

    def handle_begin_contact(self, contact, other_entity):
        if isinstance(other_entity, PlayerEntity):
            # ball is colliding with the hole but is not entirely in the hole
            self._ball_near_hole = True
            # hold onto a reference to the player ball
            self._player_ent = other_entity

    def handle_end_contact(self, contact, other_entity):
        if isinstance(other_entity, PlayerEntity):
            # ball has left the hole
            self._ball_near_hole = False

    def update(self, delta):
        # ball is currently colliding with the hole
        if self._ball_near_hole:
            a = self.body.position
            b = self._player_ent.body.position

            # ball is inside the whole
            if math.hypot(a[0] - b[0], a[1] - b[1]) <= 0.6:
                self._ball_near_hole = False

                self.handle_player_in_hole()

    def handle_player_in_hole(self):
        self.map.level_complete()
