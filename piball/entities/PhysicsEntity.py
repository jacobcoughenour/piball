from pygame import draw
from PyTMX.pytmx import (TiledObject)

from Box2D import b2Contact

from piball.core.BaseEntity import BaseEntity

from piball.core.constants import Constants

from pygame import Rect

import math


class PhysicsEntity(BaseEntity):

    @staticmethod
    def editor_color() -> str:
        return "#000000"

    @staticmethod
    def default_props() -> dict:
        return {
            "color": "#ffff00ff",
            "circle": False
        }

    def __init__(self, tmx_object: TiledObject):
        BaseEntity.__init__(self, tmx_object)

        # get the physics world scale
        world_scale = float(Constants.physics_world_scale)

        # convert our rotation
        self.physics_rotation = math.radians(self.rotation)

        self._next_physics_position = None
        self._next_physics_linearVelocity = None
        self._next_physics_angularVelocity = None

        # top left of our tmx object converted to physics space
        top_left = (
            float(tmx_object.x) / world_scale,
            float(tmx_object.y) / world_scale
        )

        # the tmx object size converted to physics space
        size = (
            float(tmx_object.width) / world_scale,
            float(tmx_object.height) / world_scale
        )

        # rotate the top left around the center to get the correct rotated position
        pos = BaseEntity.rotate_point(
            top_left,
            (top_left[0] - (size[0] * 0.5), top_left[1] - (size[1] * 0.5)),
            self.rotation
        )

        # create our own rect just for physics.
        # we can't use pygame.rect since we are dealing with floats.
        # todo: maybe make our own Rect or Transform data type to handle this?
        self.physics_rect = [
            pos[0], pos[1],
            size[0], size[1]
        ]

        # get the color hex string from props
        hex = self.properties["color"]

        self.color = (int(hex[3:5], 16), int(hex[5:7], 16), int(hex[7:9], 16))
        """
        self.properties["color"] parsed to (r,g,b)
        """

        # draw our object shape to the entity sprite
        self.draw_sprite()

    def __post_load__(self):

        # attach a reference of this entity to the physics body and its fixtures
        # so we know what entity to call for collision events.
        if hasattr(self, "body"):
            self.body.userData = {
                "parent_entity": self
            }
            for fix in self.body.fixtures:
                fix.userData = {
                    "parent_entity": self
                }

    def destroy(self):

        # remove our physics body and its fixtures from the b2world
        if hasattr(self, "body"):
            self.b2world.DestroyBody(self.body)

        pass

    def handle_begin_contact(self, contact: b2Contact, other_entity: 'PhysicsEntity'):
        """
        Called when a fixture from this entity collides with another.

        This is called on both of the colliding entities.

        :type contact: b2Contact
        :param contact: contact info from Box2D
        :type other_entity: PhysicsEntity
        :param other_entity: the other entity this one is colliding with
        :return: None
        """
        pass

    def handle_end_contact(self, contact: b2Contact, other_entity: 'PhysicsEntity'):
        """
        Called when a fixture from this entity stops colliding with another.

        This is called for both of the colliding entities.

        :type contact: b2Contact
        :param contact: contact info from Box2D
        :type other_entity: PhysicsEntity
        :param other_entity: the other entity this one was colliding with
        :return: None
        """
        pass

    def update(self, delta):
        """
        Updates the entity position and rotation to match its physics body.

        :param delta: previous frame time in milliseconds
        :return: None
        """
        super().update(delta)

        if not hasattr(self, "body"):
            return

        if self._next_physics_position:
            self.body.position = self._next_physics_position
            self._next_physics_position = None

        if self._next_physics_linearVelocity:
            self.body.linearVelocity = self._next_physics_linearVelocity
            self._next_physics_linearVelocity = None

        if self._next_physics_angularVelocity:
            self.body.angularVelocity = self._next_physics_angularVelocity
            self._next_physics_angularVelocity = None

        # update all our rects to match the physics body

        self.physics_rect[0] = self.body.position[0] - (self.physics_rect[2] * 0.5)
        self.physics_rect[1] = self.body.position[1] - (self.physics_rect[3] * 0.5)
        self.physics_rotation = self.body.angle

        # convert the box2d rotation from radians to degrees
        self.rotation = math.degrees(self.physics_rotation)

        # top-left of the physics body rect scaled to screen space
        point = (
            self.physics_rect[0] * Constants.physics_world_scale,
            self.physics_rect[1] * Constants.physics_world_scale
        )

        # rotate the top-left around the center
        self.rect.topleft = BaseEntity.rotate_point(
            point,
            (
                self.body.position[0] * Constants.physics_world_scale,
                self.body.position[1] * Constants.physics_world_scale
            ),
            self.rotation
        )

    def draw_sprite(self):
        """
        Draw the sprite of this physics body
        :return: None
        """

        # the prop circle == true
        # so draw a circle

        if self.properties["circle"]:
            draw.circle(
                self.default_sprite.image,
                self.color,
                (int(self.tmx_object.width * 0.5), int(self.tmx_object.height * 0.5)),
                int(min(self.tmx_object.width, self.tmx_object.height) * 0.5)
            )

        else:

            # the tmx object has a "points" attribute
            # so draw the points as a polygon

            if hasattr(self.tmx_object, "points"):

                rect_min = self.tmx_object.points[0]
                rect_max = rect_min

                verts = []

                for (x, y) in self.tmx_object.points:
                    rect_min = (min(rect_min[0], x), min(rect_min[1], y))
                    rect_max = (max(rect_max[0], x), max(rect_max[1], y))

                    verts.append((x, y))

                verts = list(
                    map(
                        lambda vert: (
                            vert[0] - rect_min[0],
                            vert[1] - rect_min[1]
                        ),
                        verts
                    )
                )

                self.sprite_offset = (
                    rect_min[0] - self.tmx_object.points[0][0],
                    rect_min[1] - self.tmx_object.points[0][1],
                )

                if self.tmx_object.closed:
                    draw.polygon(
                        self.default_sprite.image,
                        self.color,
                        verts,
                        0
                    )
                else:
                    draw.lines(
                        self.default_sprite.image,
                        self.color,
                        False,
                        verts,
                        3
                    )

            # the circle prop is False and the tmx object doesn't have a "points"
            # attribute, so just draw a bounding rectangle

            else:

                draw.polygon(
                    self.default_sprite.image,
                    self.color,
                    [
                        (0, 0),
                        (self.tmx_object.width, 0),
                        (self.tmx_object.width, self.tmx_object.height),
                        (0, self.tmx_object.height)
                    ],
                    0
                )

        # debug: origin point

        # draw.polygon(
        #     self.sprite.image,
        #     (120, 200, 255),
        #     [
        #         (0, 0),
        #         (2, 0),
        #         (2, 2),
        #         (0, 2)
        #     ],
        #     0
        # )
