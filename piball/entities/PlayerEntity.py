from PyTMX.pytmx import (TiledObject)

from piball.entities.DynamicPhysicsEntity import DynamicPhysicsEntity
from piball.core.constants import Constants


class PlayerEntity(DynamicPhysicsEntity):
    """
    Player "Ball" entity controlled by the user.
    """

    @staticmethod
    def editor_color() -> str:
        return "#7C4DFF"

    @staticmethod
    def default_props() -> dict:
        return {
            "circle": True,
            "physics_density": 0.1,
            "physics_friction": 0.1,
            "color": "#ff7C4DFF"
        }

    def __init__(self, tmx_object: TiledObject):
        super().__init__(tmx_object)

        self.start = (self.rect.centerx, self.rect.centery)

    def respawn(self):
        """ teleport player to starting position"""
        self.teleport(self.start[0], self.start[1])
