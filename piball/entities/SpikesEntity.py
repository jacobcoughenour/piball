from Box2D import (b2PolygonShape, b2CircleShape, b2_staticBody, b2Contact)
from PyTMX.pytmx import (TiledObject)
from pygame import draw

from piball.entities.StaticPhysicsEntity import StaticPhysicsEntity
from piball.entities.PlayerEntity import PlayerEntity
from piball.core.constants import Constants


class SpikesEntity(StaticPhysicsEntity):
    """
    Spikes entity
    """

    @staticmethod
    def editor_color() -> str:
        return "#6991ff"

    @staticmethod
    def default_props() -> dict:
        return {
            "color": "#6991FFFF",
            "count": 5
        }

    def __init__(self, tmx_object: TiledObject):
        super().__init__(tmx_object)

    def draw_sprite(self):
        self.spike_count = self.properties["count"]

        verts = [(0, self.rect.h)]

        spike_step = self.rect.w / self.spike_count

        for i in range(self.spike_count):
            verts.append(
                (i * spike_step + (spike_step / 2), 0))
            verts.append(((i + 1) * spike_step, self.rect.h))

        draw.polygon(
            self.default_sprite.image,
            self.color,
            verts,
            0
        )

    def handle_begin_contact(self, contact: b2Contact, other_entity: 'PhysicsEntity'):
        if isinstance(other_entity, PlayerEntity):
            other_entity.respawn()
