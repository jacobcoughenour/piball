from Box2D import (b2PolygonShape, b2CircleShape)
from PyTMX.pytmx import (TiledObject)

from piball.entities.PhysicsEntity import PhysicsEntity
from piball.core.constants import Constants


class StaticPhysicsEntity(PhysicsEntity):
    """
    A basic physics entity that isn't affected by gravity.
    """

    @staticmethod
    def editor_color() -> str:
        return "#00B0FF"

    @staticmethod
    def default_props() -> dict:
        return {
            "color": "#ff00B0FF"
        }

    def __init__(self, tmx_object: TiledObject):
        super().__init__(tmx_object)

        # if the object is a circle

        if self.properties["circle"]:
            shape = b2CircleShape(
                radius=(self.physics_rect[2] * 0.5)
            )
            position = (
                self.physics_rect[0] + (self.physics_rect[2] * 0.5),
                self.physics_rect[1] + (self.physics_rect[3] * 0.5)
            )

        else:

            # if the object is a polygon

            if hasattr(self.tmx_object, "points"):

                rect_min = self.tmx_object.points[0]
                rect_max = rect_min

                verts = []

                for (x, y) in self.tmx_object.points:
                    rect_min = (min(rect_min[0], x), min(rect_min[1], y))
                    rect_max = (max(rect_max[0], x), max(rect_max[1], y))

                    verts.append((x, y))

                center = (
                    (rect_max[0] - rect_min[0]) * 0.5,
                    (rect_max[1] - rect_min[1]) * 0.5
                )

                verts = list(
                    map(
                        lambda vert: (
                            (vert[0] - rect_min[0] - center[0] + self.sprite_offset[0]) / Constants.physics_world_scale,
                            (vert[1] - rect_min[1] - center[1] + self.sprite_offset[1]) / Constants.physics_world_scale
                        ),
                        verts
                    )
                )

                shape = b2PolygonShape(
                    vertices=verts
                )
                position = (
                    self.physics_rect[0] + center[0] / Constants.physics_world_scale,
                    self.physics_rect[1] + center[1] / Constants.physics_world_scale
                )

            # if the object is a rectangle

            else:
                shape = b2PolygonShape(
                    box=(self.physics_rect[2] * 0.5, self.physics_rect[3] * 0.5)
                )
                position = (
                    self.physics_rect[0] + (self.physics_rect[2] * 0.5),
                    self.physics_rect[1] + (self.physics_rect[3] * 0.5)
                )

        # create the static physics body

        self.body = self.b2world.CreateStaticBody(
            position=position,
            angle=self.physics_rotation,
            shapes=shape
        )
