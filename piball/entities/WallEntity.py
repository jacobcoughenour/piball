from Box2D import (b2PolygonShape, b2CircleShape, b2BodyDef, b2FixtureDef, b2_staticBody)
from PyTMX.pytmx import (TiledObject, TiledObjectGroup)
from pygame import draw

from piball.entities.PhysicsEntity import PhysicsEntity
from piball.core.constants import Constants


class WallEntity(PhysicsEntity):
    """
    Physics entity for level walls
    """

    @staticmethod
    def editor_color() -> str:
        return "#00B0FF"

    @staticmethod
    def default_props() -> dict:
        return {
            "color": "#ff00B0FF"
        }

    def __init__(self, tmx_object: TiledObject, tmx_object_group: TiledObjectGroup):
        self.tmx_object_group = tmx_object_group

        tmx_object.width = Constants.screen_width
        tmx_object.height = Constants.screen_height

        super().__init__(tmx_object)

        body = self.b2world.CreateBody(
            type=b2_staticBody,
            position=(0, 0)
        )

        # rect_min = self.tmx_object.points[0]
        # rect_max = rect_min

        for obj in tmx_object_group:

            verts = list(
                map(
                    lambda vert: (
                        (vert[0]) / Constants.physics_world_scale,
                        (vert[1]) / Constants.physics_world_scale
                    ),
                    obj.points
                )
            )

            try:
                body.CreateFixture(shape=b2PolygonShape(
                    vertices=verts
                ))
            except BaseException as e:
                print(
                    "PiballMap Load: skipping invalid wall points for object #" + str(
                        obj.id))

        self.body = body

        position = (
            0, 0
        )

        # create the static physics body

        # self.body = self.b2world.CreateStaticBody(
        #     position=position,
        #     angle=self.physics_rotation,
        #     shapes=shapes
        # )

    def update(self, delta):
        pass

    def draw_sprite(self):

        for obj in self.tmx_object_group:
            draw.polygon(
                self.default_sprite.image,
                self.color,
                list(
                    map(
                        lambda vert: (
                            (vert[0]),
                            (vert[1])
                        ),
                        obj.points
                    )
                ),
                0
            )
