__all__ = [
    "DynamicPhysicsEntity",
    "StaticPhysicsEntity",
    "PlayerEntity",
    "HoleEntity",
    "SpikesEntity"
]
"""These are the entity types available to the Tiled Editor"""
