import math
import sys
import os
import json
import pymongo
from dotenv import load_dotenv
import requests


import pygame
import pygame.display as display
import pygame.event as pygame_event
import pygame.time as pygame_time

from Box2D import (b2World)
from pygame import draw
from pygame import (init, quit, KEYDOWN, QUIT, K_ESCAPE, K_RETURN, K_BACKSPACE)
from pygame import transform
from pygame.surface import Surface
from pygame import font

from piball.core.InputManager import InputManager
from piball.core.PiballMapCanvas import PiballMapCanvas
from piball.core.constants import Constants
from piball.core.GameState import GameState
from piball.core.BaseEntity import BaseEntity
from piball.core.ContactListener import ContactListener
from piball.core.MainMenu import MainMenu
from piball.core.ResultsMenu import ResultsMenu

# handle loading levels directly from the command line arguments
# this is used when launching from tiled

arg = str(sys.argv[-1:][0])
if arg.endswith(".tmx"):
    # start at the argument level path instead of the main menu
    start_level = arg[len(os.getcwd()) + 1:]
else:
    # no level path argument
    start_level = None


def start():
    load_dotenv()
    player_score = {}
    """
    starts the game
    """

    user_name = ''

    # Dimensions cannot be tuples, it breaks pytmx
    width = Constants.screen_width
    height = Constants.screen_height

    # initialize pygame
    init()

    # initialize pygame font module
    font.init()

    # create clock
    clock = pygame_time.Clock()

    # create screen surface to draw to
    screen = display.set_mode([width, height])

    # create map loader
    game_map = PiballMapCanvas()

    # playlist completion callback
    # the time param is the total elapsed time it took to complete the playlist
    def playlist_completed(time):

        results_menu.set_time(time)

        Constants.game_state = GameState.Complete

    # main menu start button callback
    def main_menu_start():

        Constants.game_state = GameState.Playing
        game_map.start_playlist(
            Constants.intro_levels,
            physics_world,
            playlist_completed
        )

    # create main menu
    main_menu = MainMenu(main_menu_start)

    def results_menu_restart(user_name, score):

        player_score = {

            "name": user_name,
            "score": score
        }

        r = requests.post("https://piball.herokuapp.com/score/new", player_score, timeout=5)
        print(r.text)

        Constants.game_state = GameState.MainMenu

    # create results menu
    results_menu = ResultsMenu(results_menu_restart)

    # create physics world
    physics_world = b2World(
        gravity=(0, 0),
        doSleep=False,
        contactListener=ContactListener(game_map)
    )

    if start_level:

        # if the start level is in the intro playlist
        # then we start the playlist at that level
        if start_level in Constants.intro_levels:
            game_map.start_playlist(
                Constants.intro_levels[Constants.intro_levels.index(start_level):],
                physics_world,
                playlist_completed
            )

            # update the game state
            Constants.game_state = GameState.Playing

    else:
        Constants.game_state = GameState.MainMenu

    # physics settings
    time_step = Constants.physics_timestep
    vel_iters = Constants.physics_vel_iters
    pos_iters = Constants.physics_pos_iters

    # fps limit
    fps = Constants.fps

    # screen clear color
    black = Constants.colors['black']

    # input manager for handling all the input
    inputmanager = InputManager()

    # main game loop

    while Constants.game_state is not GameState.Exiting:

        # update window title
        display.set_caption('PiBall - fps: ' + str(int(clock.get_fps())))

        # clear the screen
        screen.fill(black)

        tick_delta = clock.tick(fps)

        game_state = Constants.game_state

        # only run if playing a level
        if game_state is GameState.Playing:
            # get tilt from input
            tilt = inputmanager.get_tilt()

            # update world gravity to match tilt
            physics_world.gravity = (tilt[0] * Constants.physics_gravity_scale,
                                     tilt[1] * Constants.physics_gravity_scale)

            # update physics world
            physics_world.Step(time_step, vel_iters, pos_iters)

            # clear any applied forces
            physics_world.ClearForces()

            # update map and entities
            game_map.update(tick_delta)

            # draw map with entities
            game_map.draw(screen)

        elif game_state is GameState.MainMenu:
            main_menu.draw(screen)

        elif game_state is GameState.Complete:
            results_menu.draw(screen)

        # main event handler
        for event in pygame_event.get():
            # quit if window is exited
            if event.type == QUIT:
                Constants.game_state = GameState.Exiting
                break
            if event.type == KEYDOWN:
                # quit if esc is pressed
                if event.key == K_ESCAPE:
                    Constants.game_state = GameState.Exiting
                    break
            # pass events to the current menu
            if game_state is GameState.MainMenu:
                main_menu.handle_event(event)
            elif game_state is GameState.Complete:
                results_menu.handle_event(event)

        # draw to display
        display.flip()
        display.update()

    # exit when while loop is broken
    quit()
